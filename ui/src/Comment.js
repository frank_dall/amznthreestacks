import React, { Component } from 'react';

const API_URL = process.env.REACT_APP_API_URL || ""

const Card = ({ item, handleSubmit, handleEdit, handleDelete, handleCancel }) => {
   const { title, content, editMode } = item;

   if (editMode) {
       return (
           <div className="card mt-4">
               <div className="card-body">
                   <form onSubmit={handleSubmit}>
                       <input type="hidden" name="id" value={item.id} />
                       <div className="input-group input-group-sm mb-3">
                           <input type="text" name="title" className="form-control" placeholder="Title" defaultValue={title} />
                       </div>
                       <div className="input-group input-group-sm mb-3">
                           <textarea name="content" className="form-control" placeholder="Content" defaultValue={content}></textarea>
                       </div>
                       <button type="button" className="btn btn-outline-secondary btn-sm" onClick={handleCancel}>Cancel</button>
                       <button type="submit" className="btn btn-info btn-sm ml-2">Save</button>
                   </form>
               </div>
           </div>
       )
   } else {
       return (
           <div className="card mt-4">
               <div className="card-body">
                   <h5 className="card-title">{title || "No Title"}</h5>
                   <p className="card-text">{content || "No Content"}</p>
                   <button type="button" className="btn btn-outline-danger btn-sm" onClick={handleDelete}>Delete</button>
                   <button type="submit" className="btn btn-info btn-sm ml-2" onClick={handleEdit}>Edit</button>
               </div>
           </div>
       )
   }
}

class Comment extends Component {
   constructor(props) {
       super(props);
       this.state = { data: [] };
   }

   componentDidMount() {
       this.getComments();
   }

   getComments = async () => {
       const response = await fetch(`${API_URL}/comments`);
       const data = await response.json();
       data.forEach(item => item.editMode = false);
       this.setState({ data })
   }

   addNewComment = () => {
       const data = this.state.data;
       data.unshift({
           editMode: true,
           title: "",
           content: ""
       })
       this.setState({ data })
   }

   handleCancel = async () => {
       await this.getComments();
   }

   handleEdit = (commentId) => {
       const data = this.state.data.map((item) => {
           if (item.id === commentId) {
               item.editMode = true;
           }
           return item;
       });
       this.setState({ data });
   }

   handleDelete = async (commentId) => {
       await fetch(`${API_URL}/comments/${commentId}`, {
           method: 'DELETE',
           headers: {
               'content-type': 'application/json',
               accept: 'application/json',
           },
       });
       await this.getComments();
   }

   handleSubmit = async (event) => {
       event.preventDefault();
       const data = new FormData(event.target);

       const body = JSON.stringify({
           title: data.get('title'),
           content: data.get('content'),
       });

       const headers = {
           'content-type': 'application/json',
           accept: 'application/json',
       };

       if (data.get('id')) {
           await fetch(`${API_URL}/comments/${data.get('id')}`, {
               method: 'PUT',
               headers,
               body,
           });
       } else {
           await fetch(`${API_URL}/comments`, {
               method: 'POST',
               headers,
               body,
           });
       }
       await this.getComments();
   }

   render() {
       return (
            <div className="Comment">
            <button className="btn btn-primary btn-lg" href="#" onClick={this.addNewComment}>Leave a comment &raquo;</button>
            {
                this.state.data.length > 0 ? (
                    this.state.data.map(item =>
                        <Card key={item.id} item={item}
                            handleSubmit={this.handleSubmit}
                            handleEdit={this.handleEdit.bind(this, item.id)}
                            handleDelete={this.handleDelete.bind(this, item.id)}
                            handleCancel={this.handleCancel}
                        />)
                ) : (
                        <div className="card mt-5 col-sm">
                            <div className="card-body">You don't have any comments. Use the "Leave a comment" button to add some new comments!</div>
                        </div>
                    )
            }
           </div >
       );
   }
}

export default Comment;