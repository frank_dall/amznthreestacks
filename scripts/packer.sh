sudo apt update -y
sudo apt install -y nodejs npm
sudo mv /tmp/build /opt/app

sudo tee /tmp/awslogs.conf <<EOF
[general]
state_file = /var/awslogs/state/agent-state

[/opt/app/*.log]
file = /opt/app.output.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = end_of_file
log_group_name = ats
EOF

sudo chmod 666 /tmp/awslogs.conf
curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
sudo python ./awslogs-agent-setup.py --region us-east-1 -c /tmp/awslogs.conf -n
rm ./awslogs-agent-setup.py

sudo tee /etc/systemd/system/app.service <<EOF
[Service]
WorkingDirectory=/opt/app
After=cloud-init.service
Wants=cloud-init.service
ExecStart=/usr/bin/node /opt/app/api/server.js > /opt/app/output.log 2>&1
Restart=always
User=ubuntu
Group=ubuntu

[Install]
WantedBy=multi-user.target
EOF

touch /opt/app/output.log
sudo systemctl daemon-reload
sudo systemctl enable app
