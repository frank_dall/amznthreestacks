DO
$body$
  BEGIN
    IF NOT EXISTS (SELECT * from information_schema.TABLES WHERE TABLE_NAME = 'posts') THEN
      RAISE NOTICE '>>> CREATING "posts" TABLE';
      CREATE TABLE IF NOT EXISTS posts (
        ID SERIAL PRIMARY KEY,
        title TEXT,
        content TEXT
      );
      RAISE NOTICE '>>> CREATED TABLE "posts"';
    ELSE 
      RAISE NOTICE '>>> TABLE "posts" CREATED';
    END IF;
  END;
$body$;
