#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

npm install
node -e 'console.log(JSON.stringify({"database": process.env.ATS_DB,"host": process.env.ATS_HOST,"user": process.env.ATS_USER,"password": process.env.ATS_PASS}))' > settings.json

rm -rf build/
rsync -r . build

# build AMI
# packer build -var "Name=ats" scripts/packer.json

cd scripts/terraform
terraform init -input=false -no-color -force-copy -backend-config="key=ats/terraform.tfstate"
terraform get -update
terraform apply -auto-approve
