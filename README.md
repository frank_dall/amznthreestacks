# personal-space

*AKA: amzn3stacks, fdallezo/3s-\*, YAKL/YAKD (Yet another k8s lab or deployment).*

An over-architected Single-Page Application (SPA). For AWS enlightenment.

NOTE: due to the expense of RDS and EC2, the functionality on https://personalspace.show/ will not be operational in most cases.

## Development

The UI and API application components were written and tested against Node.js 11.14, but the apps should be backward compatible. I recommend using [NVM](https://github.com/creationix/nvm), which is a nice utility to manage Node versions.
It is also recommended that [Docker and docker-compose](https://www.docker.com/products/docker-desktop) is installed for your OS.

This application stack has since been refactored into separate `ui` and `api` directories within the same project.

The API service requires either a `settings.json` based on the `example.settings.json` in the root of the project, or environment variables that are set which can be referenced in the `api/server.js` file.

### Running
Once the API service has been configured, NPM scripts have been updated to launch each project independently:
```bash
cd api/
npm run start
cd ui/
npm run start
```

### Kubernetes
If you have a running Kubernetes cluster, both UI and API components can be deployed via [`helm`](https://github.com/helm/helm).

The associated `helm/app/*.values.yaml` files can be modified with custom image repositories and hostnames.

When building the UI container image, be sure to supply the API server URL as a build argument before publishing the container image to a repository.

Example:
```bash
docker build --build-arg api_server=http://api.dallfran.tam.myinstance.com -t fdallezo/3s-ui .
```
Both deployments are launched with a `build-robot` service account on the k8s cluster, where in the case of AWS, you can use a [custom IAM role](https://docs.aws.amazon.com/eks/latest/userguide/specify-service-account-role.html) to grant access to other AWS resources.

##### Note
You will need to modify `helm/app/values.api.yaml` under the `service_account.role_arn` key to implement this functionality.

#### Installation
Assuming your kubernetes config is properly configured, the deployments can be installed on your cluster by running (from the root of the project):
```bash
cd helm/app/
helm install -f api.values.yaml --name api helm/
helm install -f ui.values.yaml --name ui helm/
```
