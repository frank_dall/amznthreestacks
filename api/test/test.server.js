const assert = require('assert');
const clients = require('restify-clients');

const client = clients.createJsonClient({
    url: 'http://localhost:8080'
});

client.get('/ping', function(err, req, res, obj) {
    assert.ifError(err);
    console.log('Server returned: %j', obj);
});