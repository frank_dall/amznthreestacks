let config = {}
try {
    config = require('../settings')
} catch (e) {
    config = { "database": process.env.DATABASE_NAME, "host": process.env.DATABASE_HOST, "user": process.env.DATABASE_USER, "password": process.env.DATABASE_PASSWORD }
}
const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const bunyan = require('bunyan');
const Services = require('./services');

// log
const log = bunyan.createLogger({
    name: 'amznThreeStacks',
    level: 'info'
});
const server = restify.createServer({
    log: log
});
const cors = corsMiddleware({
    origins: ["*"],
    allowHeaders: ["Authorization"],
    exposeHeaders: ["Authorization"]
});
// plugins
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.requestLogger({ serializers: bunyan.stdSerializers }));

const services = new Services(config);

// routes
server.get('/comments', services.getComments);
server.post('/comments', services.addComment);
server.del('/comments/:id', services.deleteComment);
server.put('/comments/:id', services.updateComment);
server.head('/comments', (req, res, next) => {
    res.send(204);
    return next();
});
server.get('/ping', function(req, res, next) {
    res.send(204);
    return next();
});

server.listen(8080, function() {
    log.info('%s listening at %s', server.name, server.url);
});