const Pool = require('pg').Pool
const errors = require('restify-errors');

class Services {
    constructor(config) {
        this.pool = new Pool({
            user: config.user,
            password: config.password,
            host: config.host,
            database: config.database,
            port: 5432,
        });
        this.getComments = (req, res, next) => {
            req.log.info('getComments');
            this.pool.query('SELECT * FROM posts ORDER BY id ASC', (error, results) => {
                if (error) {
                    req.log.fatal('error during getComments: ' + error.message);
                    return next(new Error('during getComments'));
                }
                req.log.trace('successfully retrieved posts');
                res.header('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1
                res.header('Pragma', 'no-cache'); // HTTP 1.0
                res.header('Expires', '0'); // Proxies
                res.json(200, results.rows);
            });
            return next();
        };
        this.addComment = (req, res, next) => {
            req.log.info('addComment');
            const title = req.body.title,
                content = req.body.content;
            if (!title || !content) {
                return next(new errors.InvalidContentError());
            }
            this.pool.query('INSERT INTO posts (title, content) VALUES ($1, $2)', [title, content], (error, results) => {
                if (error) {
                    req.log.fatal('error during addComment');
                    return next(new Error('during addComment'));
                }
                res.json(200, { title, content });
            });
            return next();
        };
        this.deleteComment = (req, res, next) => {
            const id = parseInt(req.params.id);
            this.pool.query('DELETE FROM posts WHERE id = $1', [id], (error, results) => {
                if (error) {
                    req.log.fatal('error during deleteComment');
                    return next(new Error('during deleteComment'));
                }
                req.log.info(`deleted comment with id: ${id}`);
                res.send(204);
            });
        };
        this.updateComment = (req, res, next) => {
            const id = parseInt(req.params.id);
            const title = req.body.title,
                content = req.body.content;
            if (!title || !content) {
                return next(new errors.InvalidContentError());
            }
            this.pool.query('UPDATE posts SET title = $1, content = $2 WHERE id = $3', [title, content, id], (error, results) => {
                if (error) {
                    req.log.fatal('error during updateComment');
                    return next(new Error('during updateComment'));
                }
                req.log.info('updateComment success');
                res.json(200, `updated comment with id: ${id}`);
            });
        };
    }
}
module.exports = Services;